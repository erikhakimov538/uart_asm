     .equ	GPIO_PORTA_BASE,         0x40080000
     .equ	GPIO_PORTB_BASE,         0x40088000
     .equ	GPIO_PORTC_BASE,         0x40090000
     .equ MDR_RST_CLK_BASE,        0x40020000
     .equ MDR_UART1_BASE,          0x40008000
     .equ MDR_UART2_BASE,          0x40010000
     .equ SSP_BASE,                0x40000000
     .equ MCP32_BASE,              0x40

     .equ DR,               0x008
     .equ GPIOB,            0x13
