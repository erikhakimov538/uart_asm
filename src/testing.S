     .syntax	unified
	 .cpu cortex-m0

     .text

     .thumb
     .thumb_func
     .align 2
     .globl   test_print_and_div
     .type    test_print_and_div, %function

test_print_and_div:
/* r0 - делимое, r1 - делитель, r2 - ожид. результат */
/* формат вывода: делимое, делитель, результат*/
    push {r0, r1, r2, r3, lr}
    mov r2, r1
    mov r1, r0
    bl print_hex

    ldr r1, =0x20
    bl uart_send_and_wait_busy

    mov r1, r2
    bl print_hex

    ldr r1, =0x20
    bl uart_send_and_wait_busy

    mov r1, r2
    bl integer_divide_v3

    mov r2, r1
    mov r1, r0
    bl print_hex

    ldr r1, =0x20
    bl uart_send_and_wait_busy

    mov r1, r2
    bl print_hex

    mov r3, r0
    mov r4, r1
    pop {r0, r1, r2}

    cmp r3, r2
    bne error
    ldr r1, =0x2B
    bl uart_send_and_wait_busy

    cmp r4, r5
    bne error
    ldr r1, =0x2B
    bl uart_send_and_wait_busy
    b func_out

    error:
    ldr r1, =0x21
    bl uart_send_and_wait_busy

    func_out:
    ldr r1, =0x0A
    bl uart_send_and_wait_busy
    ldr r1, =0x0D
    bl uart_send_and_wait_busy
    pop {r3, pc}



test_div1:
    push {lr}
    ldr r0, =7
    ldr r1, =2
    ldr r2, =3
    ldr r5, =1

    bl test_print_and_div
    pop {pc}

test_div2:
    push {lr}
    ldr r0, =512
    ldr r1, =16
    ldr r2, =32
    ldr r5, =0

    bl test_print_and_div
    pop {pc}

test_div3:
    push {lr}
    ldr r0, =0xAAAAAAAA
    ldr r1, =0x00005555
    ldr r2, =0x00020002
    ldr r5, =0

    bl test_print_and_div
    pop {pc}

test_div4:
    push {lr}
    ldr r0, =243
    ldr r1, =24
    ldr r2, =10
    ldr r5, =3

    bl test_print_and_div
    pop {pc}

test_div5:
    push {lr}
    ldr r0, =0xFFFFFFFF
    ldr r1, =0x12345678
    ldr r2, =14
    ldr r5, =0x123456F

    bl test_print_and_div
    pop {pc}

test_div6:
    push {lr}
    ldr r0, =0x12345678
    ldr r1, =0x12345678
    ldr r2, =1
    ldr r5, =0

    bl test_print_and_div
    pop {pc}

test_send_buffer:
    push {lr}
    ldr r0, =uart_buffer
    ldr r1, =0
    ldr r2, =1
    ldr r3, =0x30
    loop3:
    strb r3, [r0, r1]
    adds r1, #1
    adds r3, #1
    cmp r1, #10 
    bne loop3
    bl uart_send_buffer
    
    pop {pc}

test_decimal:
    push {lr}
    ldr r0, =125
    ldr r1, =7

    bl integer_divide_v3
    mov r1, r0

    bl print_dec
    pop {pc}

test_decimal2:
    push {lr}
    ldr r1, =3

    bl print_dec
    pop {pc}

test_decimal3:
    push {lr}
    ldr r1, =0xFFFFFFFF

    bl print_dec
    pop {pc}

test_bin:
    push {lr}
    ldr r0, =73423
    ldr r1, =4

    bl integer_divide_v3
    mov r1, r0

    bl print_bin
    pop {pc}

test_bin2:
    push {lr}
    ldr r1, =1734

    bl print_bin
    pop {pc}

test_bin3:
    push {lr}

    ldr r1, =0xFFFFFFFF

    // bl print_bin
    bl print_hex

    pop {pc}

     .thumb
     .thumb_func
     .align 2
     .globl   test_all
     .type    test_all, %function

test_all:
    push {lr}
    bl test_div1
    bl test_div2
    bl test_div3
    bl test_div4
    bl test_div5
    bl test_div6
    bl test_send_buffer
    bl test_decimal
    bl test_decimal2
    bl test_decimal3
    bl test_bin
    bl test_bin2
    bl test_bin3

    pop {pc}

    .end 
